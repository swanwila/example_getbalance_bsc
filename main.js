const Web3 = require('web3');
const wallet_address = "";

(async ()=>{
    const web3 = new Web3("https://bsc-dataseed1.binance.org:443");
    let balanceWei = await web3.eth.getBalance(wallet_address);
    let balanceRaw = Web3.utils.fromWei(balanceWei);
    console.log(balanceRaw + " WBNB");
})()